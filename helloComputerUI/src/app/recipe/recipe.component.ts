import {Component, OnInit, OnDestroy} from '@angular/core';
import {HttpClient} from '@angular/common/http';
@Component({
    selector: 'app-recipe',
    templateUrl: './recipe.component.html',
    styleUrls: ['./recipe.component.css']
})
export class RecipeComponent implements OnInit, OnDestroy {

    private recipeUrl: string = 'http://localhost:3000/api/recipe';
    recipes: any;
    originalRecipes: any;
    searchText;
    subscription: any;

    constructor(private _http: HttpClient) {
    }

    ngOnInit() {
        this._http.get(this.recipeUrl).subscribe(res => {
            this.originalRecipes = res;
            this.recipes = res;
        });
    }

    ngOnDestroy(): void {
        if (this.subscription)
            this.subscription.unsubscribe();
    }

}
