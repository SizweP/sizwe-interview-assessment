import {Component, OnInit, OnDestroy} from '@angular/core';
import {ContactDetail} from './contact-detail';
import {HttpClient} from '@angular/common/http';

@Component({
    selector: 'app-contact',
    templateUrl: './contact.component.html',
    styleUrls: ['./contact.component.css']
})
export class ContactComponent implements OnInit, OnDestroy {

    private contact: ContactDetail;
    private success: boolean = false;
    private subscription: any;

    constructor(private _http: HttpClient) {
        this.contact = new ContactDetail();
    }

    ngOnInit() {
    }

    submit(f): void {
        console.log(f);
        if (!f.valid)
            return;

        this.subscription = this._http.post('http://localhost:3000/api/enquiry/addEnquiry', this.contact)
            .subscribe(res => {
                this.success = true;
            })
    }

    ngOnDestroy(): void {
        if (this.subscription)
            this.subscription.unsubscribe();
    }

}
