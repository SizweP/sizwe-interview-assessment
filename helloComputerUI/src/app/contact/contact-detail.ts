export class ContactDetail {

  name: string;
  email: string;
  phone: string;
  message: string;

  constructor() { }
}