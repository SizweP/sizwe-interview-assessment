var async = require('async'),
	keystone = require('keystone');

var Recipe = keystone.list('Recipe');

exports.list = function(req, res) {
	Recipe.model.find(function(err, items) {

		if (err) 
			return res.err('database error', err);

		res.send({
			recipes: items
		});

	});
}
