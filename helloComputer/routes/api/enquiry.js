var async = require('async'),
	keystone = require('keystone');

var Enquiry = keystone.list('Enquiry');

exports.addEnquiry = function(req, res) {

	var newEnquiry = new Enquiry.model(req.body);
	newEnquiry.save(function(err, task) {
		if (err)
			res.send(err);
		res.json(task);
	});
};
