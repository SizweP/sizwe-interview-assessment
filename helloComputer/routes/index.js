/**
 * This file is where you define your application routes and controllers.
 *
 * Start by including the middleware you want to run for every request;
 * you can attach middleware to the pre('routes') and pre('render') events.
 *
 * For simplicity, the default setup for route controllers is for each to be
 * in its own file, and we import all the files in the /routes/views directory.
 *
 * Each of these files is a route controller, and is responsible for all the
 * processing that needs to happen for the route (e.g. loading data, handling
 * form submissions, rendering the view template, etc).
 *
 * Bind each route pattern your application should respond to in the function
 * that is exported from this module, following the examples below.
 *
 * See the Express application routing documentation for more information:
 * http://expressjs.com/api.html#app.VERB
 */

var keystone = require('keystone');
var middleware = require('./middleware');
var importRoutes = keystone.importer(__dirname);

// Common Middleware
keystone.pre('routes', middleware.initLocals);
keystone.pre('render', middleware.flashMessages);

// Import Route Controllers
var routes = {
	views: importRoutes('./views'),
	// api: importRoutes('./api')
};

// Setup Route Bindings
exports = module.exports = function (app) {
	var recipeList = require('./api/recipe');
	var enquiry = require('./api/enquiry');

	//allow for CORS
	app.use(function(req, res, next) {
		res.header("Access-Control-Allow-Origin", "*");
		res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
		next();
	});
	
	// Views
	app.get('/', routes.views.index);
	app.route('/api/recipe').get(recipeList.list);
	app.route('/api/enquiry/addEnquiry').all(enquiry.addEnquiry);
	
	//for unknown endpoint calls
	app.use(function(req, res) {
		res.status(404).send({url: req.originalUrl + ' not found'})
	});
};
