var keystone = require('keystone');
var Types = keystone.Field.Types;

/**
 * Enquiry Model
 * =============
 */

var Enquiry = new keystone.List('Enquiry');

Enquiry.add({
	name: { type: String, required: true },
	email: { type: Types.Email, initial: true, required: true },
	phone: { type: String, initial: true, required: true },
	message: { type: String, initial: true, required: true },
	createdAt: { type: Date, default: Date.now },
});

Enquiry.defaultSort = '-createdAt';
Enquiry.defaultColumns = 'name, email, message, createdAt';
Enquiry.register();
