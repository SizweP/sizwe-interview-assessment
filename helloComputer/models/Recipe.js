var keystone = require('keystone');
// var Types = keystone.Field.Types;

var Recipe = new keystone.List('Recipe');

Recipe.add({
	name: { type: String, required: true, unique: true, index: true },
	instructions: { type: String, initial: true, required: true}
});

Recipe.defaultColumns = 'name, instructions';
Recipe.register();
